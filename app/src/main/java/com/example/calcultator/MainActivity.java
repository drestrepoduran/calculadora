package com.example.calcultator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private EditText input_num1, input_num2;
    private TextView label_result;
    private Double num1, num2, result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        input_num1 = findViewById(R.id.inputNumberOne);
        input_num2 = findViewById(R.id.inputNumberTwo);
        label_result = findViewById(R.id.textViewResult);
    }

    public String calculate(String operation){
        if(validate()){
            num1 = Double.parseDouble(input_num1.getText().toString());
            num2 = Double.parseDouble(input_num2.getText().toString());

            switch (operation){
                case "sum":
                    result = num1 + num2;
                    break;
                case "sub":
                    result = num1 - num2;
                    break;
                case "mul":
                    result = num1 * num2;
                    break;
                case "div":
                    if(num2 == 0){
                        input_num2.setError(getString(R.string.labelErrorZero));
                        input_num2.requestFocus();
                        return "";
                    }else{
                        result = num1 / num2;
                    }

                    break;
            }

            return String.format("%.2f", result);
        }

        return "";

    }

    public void sum(View v){
        label_result.setText(""+ calculate("sum"));
    }

    public void sub(View v){
        label_result.setText(""+ calculate("sub"));
    }

    public void mul(View v){
        label_result.setText(""+ calculate("mul"));
    }

    public void div(View v){
        label_result.setText(""+ calculate("div"));
    }

    public void clear(View v){
        label_result.setText("");
        input_num1.setText("");
        input_num2.setText("");
        input_num1.requestFocus();
    }

    public boolean validate(){
        if(input_num1.getText().toString().isEmpty()){
            input_num1.setError(getString(R.string.labelError1));
            input_num1.requestFocus();
            return false;
        }

        if(input_num2.getText().toString().isEmpty()){
            input_num2.setError(getString(R.string.labelError2));
            input_num2.requestFocus();
            return false;
        }

        return true;
    }
}